

(in-package :glrepl)


(defvar *esc-pressed* nil)

(defparameter *glwindow* nil)

(defparameter *console* t)

;; evaluate a form, echo result to console: somewhat stolen
;; from perfectstorm..
(defun console-eval (s)
  "Evauluate s and return the output as a string"
  (let ((eval-result
         (format nil "~a"
                 (handler-case (multiple-value-list
                                (eval (read (make-string-input-stream s))))
                   (error (e)
                     (format nil "~a" e))))))
    eval-result))

;; -- callbacks --------------

(cffi:defcallback console-char-callback :void ((key :int) (action :int))
  ;; (format t "Char ~A " key)
  (when (and *console* (= action glfw:+press+))
    (let ((c (code-char key)))
      (when c
        (add-char *glwindow* (code-char key))))))


(cffi:defcallback console-key-callback :void ((key :int) (action :int))
  ;; (format t "Keypress ~A " key)
  (when (= action glfw:+press+)
    ;; f10 toggles console
    (when (= key glfw:+key-f10+)
      (setf *console* (not *console*)))
    ;; esc quits
    (when
        (= key glfw:+key-esc+)  (setf *esc-pressed* t))
    (when *console*
      (cond        
        ;;       ;; backspace removes previous char
        ((= key glfw:+key-backspace+) (del-char-left *glwindow*))
        ;;       ;; del removes next char
        ((= key glfw:+key-del+) (del-char-right *glwindow*))
        ((= key glfw:+key-f5+)  (progn
                                  (setf (current-result-line *glwindow*) 
                                        (console-eval (current-line-as-string *glwindow*)))
                                  (add-to-history *glwindow* (current-line-as-string *glwindow*))))
        ;; arrows move cursor
        ((= key glfw:+key-left+) (cursor-left *glwindow*))
        ((= key glfw:+key-right+) (cursor-right *glwindow*))

        ;; up down through history
        ((= key glfw:+key-up+) (setf (current-line *glwindow*) (previous-history *glwindow*)))
        ((= key glfw:+key-down+) (setf (current-line *glwindow*) (next-history *glwindow*)))
         
        ((= key glfw:+key-f6+) (setf (current-result-line *glwindow*) "Repl Result"))
        ))))



;;       ;; TO DO
;;       ;; page up, page down, skip sexps


;;       ;; return inserts newline
;;       ;; f5 evaluates -- TO DO -- sort this out


(cffi:defcallback window-size-callback :void ((width :int) (height :int))
  (setf (win-width-of *glwindow*) width)
  (setf (win-height-of *glwindow*) height)
  (viewport *glwindow*))


(defun init-gl ()
  (gl:enable gl:+texture-2d+)
  (gl:enable gl:+blend+)
  (gl:matrix-mode gl:+projection+)
  (gl:load-identity)
  (gl:push-matrix)
  (gl:matrix-mode gl:+modelview+)
  (gl:load-identity)
  (gl:push-matrix)
  (viewport *glwindow*))

(defun end-gl ()
  ())

(defun callback-set ()
  (setf *esc-pressed* nil)
  (glfw:set-key-callback (cffi:callback console-key-callback))
  (glfw:set-char-callback (cffi:callback console-char-callback)))

(defun callback-clear ()
  (glfw:set-key-callback (cffi:null-pointer))
  (glfw:set-char-callback (cffi:null-pointer)))

(defun render-char (font c x y)
  (when (graphic-char-p c)
    (let ((image (aref (glrepl::images-of font) (char-code c))))
      (when (typep  image 'rgba-image) ;; couldn't this simply be when?
        (with-opengl
          (gl:bind-texture gl:+texture-2d+
                           (cffi::mem-ref (name-of image) :uint32))
          (gl:tex-env-f gl:+texture-env+ gl:+texture-env-mode+ gl:+replace+) ;; maybe +decal+?
          (gl:color-4f 0.0 0.0 0.0 1.0)
          (gl:blend-func gl:+src-alpha+ gl:+one-minus-src-alpha+)
          (let ((texture-height (texture-height-of *glwindow*))
                (texture-width (texture-width-of *glwindow*)))
            (gl:with-begin gl:+quads+
              (gl:tex-coord-2i 0 1)
              (gl:vertex-2f x y) ;; top lhs
              (gl:tex-coord-2i 1 1)
              (gl:vertex-2f  (+ x texture-width) y  ) ;; top rhs
              (gl:tex-coord-2f 1 0)
              (gl:vertex-2f  (+ x texture-width) (+ y texture-height)) ;; bot rhs
              (gl:tex-coord-2i 0 0)
              (gl:vertex-2f x (+ y texture-height))))))))) ;; bot lhs

(defun render-cursor (font x y)
  (let ((image (aref (glrepl::images-of font) (char-code #\_))))
      (when (typep  image 'rgba-image)
        (with-opengl
          (gl:bind-texture gl:+texture-2d+
                           (cffi::mem-ref (name-of image) :uint32))
          (gl:tex-env-f gl:+texture-env+ gl:+texture-env-mode+ gl:+replace+) ;; maybe +decal+?
          (gl:color-4f 1.0 0.0 0.0 1.0)
          (gl:blend-func gl:+src-alpha+ gl:+one-minus-src-alpha+)
          (let ((texture-height (texture-height-of *glwindow*))
                (texture-width (texture-width-of *glwindow*)))
            (gl:with-begin gl:+quads+
              (gl:tex-coord-2i 0 1)
              (gl:vertex-2f x y) ;; top lhs
              (gl:tex-coord-2i 1 1)
              (gl:vertex-2f  (+ x texture-width) y  ) ;; top rhs
              (gl:tex-coord-2f 1 0)
              (gl:vertex-2f  (+ x texture-width) (+ y texture-height)) ;; bot rhs
              (gl:tex-coord-2i 0 0)
              (gl:vertex-2f x (+ y texture-height)))))))) ;; bot lhs
          
;; (defun render-world ()
;;   (multiple-value-bind
;;         (px py)
;;       (window-pixel-atxy *glwindow* 0 0)
;;   (render-char (font-of *glwindow*) #\A px py)))

(defun render-string (str x y)  
  (let ((sx x) 
        (sy y))
      (iterate
        ;; while there is still a line to render
        (for char-to-render in-string str)
        (multiple-value-bind  (pixel-x pixel-y)
            (window-pixel-atxy *glwindow* sx sy)
            ;; we have a drawble
          (when (graphic-char-p char-to-render)
            ;; actually draw our char
            (render-char (font-of *glwindow*) char-to-render pixel-x pixel-y)
            ;; advance cursor to next screen colum
            (incf sx))))))


(defparameter *console-render-debug-fn* #'(lambda ()))


(defun render-console ()
  (when *console*
    ;; set up textures, clobber projection, give us raster-style viewport
    (gl:enable gl:+texture-2d+)
    (gl:enable gl:+blend+)
    (gl:matrix-mode gl:+projection+)
    (gl:load-identity)
    (gl:matrix-mode gl:+modelview+)
    (gl:load-identity)
    (viewport *glwindow*)
    (funcall *console-render-debug-fn*)
    (let* ((rendered-chain (current-line *glwindow*))
           (screen-line 0)
           (screen-column 0)
           (current-chain-offset 0)
           (char-to-render #\Nul)
           (cursor (make-instance 'flexichain:left-sticky-flexicursor :position 0 :chain rendered-chain)))
      (declare (type fixnum  screen-line screen-column current-chain-offset))
      (iterate
        ;; while there is still a line to render
        (while (and (not (null cursor)) (< screen-line (text-height-of *glwindow*))))
        (iterate
          (while (not (flexichain:at-end-p cursor)))
          (setf char-to-render (flexichain:element> cursor))
          (multiple-value-bind  (pixel-x pixel-y)
              (window-pixel-atxy *glwindow* screen-column screen-line)

            ;; we have a drawble
            (when (graphic-char-p char-to-render)
              ;; actually draw our char
              (render-char (font-of *glwindow*) char-to-render pixel-x pixel-y)
              (when (and (zerop current-chain-offset) 
                         (= (flexichain:cursor-pos cursor) (flexichain:cursor-pos (current-cursor *glwindow*))))
                (render-cursor (font-of *glwindow*) pixel-x pixel-y))
              ;; advance cursor to next screen colum
              (incf screen-column)
              ;; word wrap if off ed
              (when (>= screen-column (text-width-of *glwindow*))
                (setf screen-column 0)
                (incf screen-line)))

            ;; we have a newline
            (when (char= #\Newline char-to-render)
              ;; first render the cursor if its at eol
              
              (setf screen-column 0)
              (incf screen-line)))

          (incf (flexichain:cursor-pos cursor))
          (finally 
           (multiple-value-bind  (pixel-x pixel-y)
               (window-pixel-atxy *glwindow* screen-column screen-line)
             
           (when (and (zerop current-chain-offset) 
                      (= (flexichain:cursor-pos cursor) (flexichain:cursor-pos (current-cursor *glwindow*))))
             (render-cursor (font-of *glwindow*) pixel-x pixel-y)))))
          
        ;; ran out of this line, bump onto next
        (incf screen-line)
        (setf screen-column 0)
        (incf current-chain-offset)
        (setf cursor nil)
        (setf rendered-chain nil)
        (let ((current-chain-index (+ (cursor-line-of *glwindow*) current-chain-offset)))
          (when (< current-chain-index (length (lines-of *glwindow*)))
            (setf rendered-chain (chain-of (aref (lines-of *glwindow*) current-chain-index)))
            (setf cursor (make-instance 'flexichain:left-sticky-flexicursor :position 0 :chain rendered-chain))))))))


(defun main-loop ()
  (let ((t0 (glfw:get-time))
        (dt 0.0)
        (test-image (make-instance 'glrepl::rgba-image :width 64 :height 64)))
    (iterate
      (for i from 0 below (* 64 64))
      (setf (glrepl::pixel test-image i) #X000000FF))
    (glrepl::update-image test-image)
    (glfw:sleep 0.05d0)
    (gl:clear-color 0.0 0.0 0.0 1.0)
    (iterate
      (while (= (glfw::get-window-param glfw:+opened+) glfw:+true+))
      (gl:clear gl:+color-buffer-bit+)
      (setf dt (- (glfw:get-time) t0))
      (setf t0 (glfw:get-time))
      (render-console)
      ;;(glrepl::render  test-image)
      ;; update
      ;; check for time available if time is avaliable render
      ;; surrender any cpu time..
      (glfw:swap-buffers)
      (if *esc-pressed*
          (glfw:close-window)))))

(defun glrepl ()
  (if (glfw::init)
      (progn
        (setf *glwindow* (make-instance 'glrepl-window))
        (add-line *glwindow*)
        (add-line *glwindow*)
        (add-string *glwindow* "(format nil \"Hello World\")")
        (if (glfw:open-window (win-width-of *glwindow*) (win-height-of *glwindow*) 16 16 16 16 16)
            (progn
              (glfw:set-window-title "OpenGL Repl")
              (progn
                (init-gl)
                (format t "Making font..")
                (setf (font-of *glwindow*) (make-font (merge-pathnames #P"VeraMono.ttf"))) ;; prbly shld be mber of window
                (format t "Done..")
                (glfw:swap-interval 1)
                (glfw:enable glfw:+key-repeat+)
                (callback-set)
                ;;                (glrepl::dump (aref *font-images* 65))
                (main-loop)
                (callback-clear)
                (end-gl)
                (glfw:close-window)
                (glfw:terminate)))
            (progn
              (glfw:terminate)
              (error "Failed to open window"))))
        (error "Failed to init glfw")))

;; (defun test-glrepl ()
;;   (glfw:with-init-window ("Glrepl" *win-width* *win-height*)
;;     (glfw:set-window-size-callback (cffi:callback window-size-callback))
;;     (glfw:set-key-callback (cffi:callback key-callback))
;;     (glfw:swap-interval 1)
;;     (let* ((frame 0))
;;       (iterate
;;         (while (and (not *esc-pressed*)
;;                     (eql (glfw:get-window-param glfw:+opened+) gl:+true+)))
;;         (gl:clear gl:+color-buffer-bit+)
;;         (incf frame)
;;         (glfw:swap-buffers)
;;           (cl:sleep 0.1))
;;       (if (eql (glfw:get-window-param glfw:+opened+) gl:+true+)
;;         (glfw:close-window))
;;       (glfw:terminate))))

;; (setf *glwindow* (make-instance 'glrepl-window))
;; (make-instance 'glrepl-window-line)

;; (add-line *glwindow*)
;; (add-string *glwindow* "Hello World")

;;(test-glrepl)
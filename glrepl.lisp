
(in-package :glrepl)


(defclass glrepl-font ()
  ((height :initform 128 :reader bitmap-height-of :type fixnum) ;; 15 up
   (width :initform 128  :reader bitmap-width-of :type fixnum) ;; 16 across
   (pathname :initform (merge-pathnames #P"VeraMono.ttf") :initarg :pathname :reader pathname-of)
   (images :initform (make-array 128) :reader images-of :type (simple-array t *))))


;; note that these are 7 bit fonts since I'm avoiding the horrors of encoding for now
;; probably the correct way to do this is to decode on the fly and cache font textures
;; but deadlines are DEADlines...


(defun make-font (font-pathname)
  ;;  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (let ((result (make-instance 'glrepl-font :pathname font-pathname)))
    (iterate
      (for i from 0 below 128)
      (declare (fixnum i))
      (vecto:with-canvas (:width (bitmap-width-of result) :height (bitmap-height-of result))
        (let ((repl-font (vecto:get-font (pathname-of result)))
              (repl-letter (string (code-char i))))
          (vecto:set-font repl-font 12)
          (when (zpb-ttf:glyph-exists-p (code-char i) repl-font)
            (handler-case
                (progn
                  (vecto:set-rgba-fill 0.0 0.0 0.0 0.0)
                  (vecto:clear-canvas)
                  (vecto:set-rgba-fill 1.0 1.0 1.0 1.0)
                  (vecto:translate 52 24) ;; to do -- these are magic numbers -- should be computed from bitmap width & height
                  (vecto:scale 12 8)
                  (vecto:draw-centered-string 0 0 repl-letter)
                  ;;                  (when (alpha-char-p (code-char i))
                  ;;                    (vecto:save-png (merge-pathnames  (concatenate 'string (string (code-char i)) ".png"))))
                  (vecto:with-graphics-state
                    (setf (aref (images-of result) i)
                          (make-instance 'rgba-image :width (bitmap-width-of result) :height (bitmap-height-of result)))
                    (iterate
                      (for x from 0 below (bitmap-width-of result))
                      (declare (fixnum x))
                      (iterate
                        (for y from 0 below (bitmap-height-of result))
                        (declare (fixnum y))
                        (setf (pixel-xy (aref (images-of result) i) x y) ;;  #XFF000000
                              (pixval
                               (the fixnum (aref (zpng::data-array (vecto::image vecto::*graphics-state*)) y x 0))
                               (the fixnum (aref (zpng::data-array (vecto::image vecto::*graphics-state*)) y x 1))
                               (the fixnum (aref (zpng::data-array (vecto::image vecto::*graphics-state*)) y x 2))
                               (the fixnum (aref (zpng::data-array (vecto::image vecto::*graphics-state*)) y x 3)))
                              )))
                    (update-image (aref (images-of result) i))))
              (error () (format t  "Skipping char ~A~&" i)))))))
    result))

(defun destroy-font (font)
  (iterate
    (for i from 0 below 128)
    (when (typep (aref (images-of font) i) 'rgba-image)
      (destroy-image (aref (images-of font) i))
      (setf (aref (images-of font) i) NIL))))

;; to do -- check bounding boxes are all sane..



;; (zpb-ttf:with-font-loader  (fnt (merge-pathnames #P"VeraMono.ttf"))
;;   (format t "Font Bounding box ~A~%" (zpb-ttf::bounding-box fnt))
;;   (iterate
;;     (for i from 0 to 128)
;;     (format t "~A ~A~%"
;;             (if (zpb-ttf:glyph-exists-p (code-char i) fnt) "Exists" "Nonexistent")
;;             (zpb-ttf::string-bounding-box (string (code-char i)) fnt))))

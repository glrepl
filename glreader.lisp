(in-package :glrepl)

(defvar eof "Eof")
(defun eof-object? (x) (eq x eof))

(defvar *glrepl-readtable* (copy-readtable))

(defun glrepl-read (&optional (stream *standard-input*))
   (let ((*readtable* *glrepl-readtable*))
     (read stream nil eof)))

 ;; use #[ ] to make pixels (noationally convenient)
 (defun |#[reader| (stream char arg)
   (declare (ignore char arg))
   `(pixval ,@(read-delimited-list #\] stream t)))
     
(set-dispatch-macro-character #\# #\[ #'|#[reader| *glrepl-readtable*)  
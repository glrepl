
(in-package :glrepl)


;; to do -- get these from alexandria or kmcrl or somesuch
(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
       `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
          ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
             ,@body)))))

(defmacro with-opengl (&body forms)
  (with-gensyms (error-sym)
    `(progn ,@forms 
            (let ((,error-sym  (gl:get-error)))
              (unless ,error-sym 
                (error "OpenGL Error ~A~%" 
                       (case ,error-sym
                         (gl:+INVALID-ENUM+ "Invalid Enum")
                         (gl:+INVALID-VALUE+ "Invalid value")
                         (gl:+INVALID-OPERATION+ "Invalid Operation")
                         (gl:+OUT-OF-MEMORY+ "Out of memory")
                         (gl:+STACK-OVERFLOW+ "Stack overflow")
                         (gl:+STACK-UNDERFLOW+ "Stack underflow"))))))))

(defclass rgba-image  ()
  ((name :accessor name-of)
   (width :accessor width-of :initform 0)
   (height :accessor height-of :initform 0)
   (format :reader format-of :initform gl:+rgba+)
   (bpp :reader bpp-of :initform 4)
   (data :accessor data-of)
   (size :accessor size-of))
  (:documentation "Data  for an opengl RGBA texture"))

(defmethod initialize-instance :after ((self rgba-image) &key width height)
  "Create a sized rgba texture"
  (setf (width-of self) width)
  (setf (height-of self) height)
  (setf (slot-value self 'name) (cffi:foreign-alloc :uint32))
  (with-opengl 
    (gl:gen-textures 1 (name-of self))
    (gl:bind-texture gl:+texture-2d+ (cffi::mem-ref (name-of self) :uint32))
    (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-wrap-s+ gl:+repeat+) 
    (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-wrap-t+ gl:+repeat+) 
    (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-mag-filter+ gl:+linear+)
    (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-min-filter+ gl:+linear+) 
    (gl:pixel-store-i gl:+unpack-alignment+ 1)
    (gl:tex-env-f gl:+texture-env+ gl:+texture-env-mode+ gl:+blend+))
  (setf (slot-value self 'data)
        (cffi:foreign-alloc :uint32
                            :count (* (width-of self) (height-of self))
                            :initial-element 0)))


(defmethod update-image ((self rgba-image))
  "Upload an RGBA texture"
  (with-opengl
    (gl:bind-texture gl:+texture-2d+ (cffi::mem-ref (name-of self) :uint32))
    (gl:tex-image-2d gl:+texture-2d+ 0 gl:+rgba+ 
                     (width-of self) (height-of self) 
                     0 gl:+rgba+ gl:+unsigned-byte+ (data-of self))))


(defgeneric render (self &key target))

(defmethod render ((self rgba-image) &key target)
  "Render an RGBA texture"
  (declare (ignore target))
  (with-opengl
    (gl:bind-texture gl:+texture-2d+ (cffi::mem-ref (name-of self) :uint32))
    (gl:tex-env-f gl:+texture-env+ gl:+texture-env-mode+ gl:+decal+)
    (gl:with-begin gl:+quads+
      (gl:tex-coord-2i 0 0)
      (gl:vertex-2f -0.5 -0.5) ;; top lhs
      (gl:tex-coord-2i 1 0)
      (gl:vertex-2f  0.5 -0.5) ;; top rhs
      (gl:tex-coord-2i 1 1)
      (gl:vertex-2f  0.5  0.5) ;; bot rhs
      (gl:tex-coord-2i 0 1)
      (gl:vertex-2f -0.5  0.5)))) ;; bot lhs

(defmethod destroy-image ((self rgba-image))
  "Release the memory used by an RGBA texture"
  (setf (width-of self) 0)
  (setf (height-of self) 0)
  (with-opengl
    (gl:delete-textures 1 (name-of self))
    (cffi:foreign-free (name-of self))
    (cffi:foreign-free (data-of self))))


(defmethod image-size ((image rgba-image))
  "Overall RGBA texture size in bytes"
  (* (width-of image) (height-of image)))

(defmethod pixel ((image rgba-image) i)
  "Access a pixel in an RGBA texture"
  (cffi:mem-aref (data-of image) :uint32 i))

(defmethod (setf pixel) (pixval (image rgba-image) i)
  "Set a pixel in an RGBA texture"
  (setf (cffi:mem-aref (data-of image) :uint32 i) pixval))

(defmethod indexxy ((image rgba-image) index)
  "Map an i index to an x,y index of a RGBA texure"
  (values (mod index (width-of image))
          (rem index (width-of image))))

(defmethod xyindex ((image rgba-image) x y)
  "Map an x,y index to an i index of a RGBA texure"
  (the (unsigned-byte 32) (+ x (* (width-of image) y))))

(defmethod pixel-xy ((image rgba-image) x y)
  "Get a pixel in an image using x y oords"
  (pixel image (xyindex image x y))) 

(defmethod (setf pixel-xy) (pixval (image rgba-image) x y)
  "Set a pixel in an image using x y oords"
  (setf (cffi:mem-aref (data-of image) :uint32 (xyindex image x y)) pixval))

(defmethod dump ((image rgba-image))
  (iterate
    (for y from 0 below (height-of image))
    (format t "~&")
    (iterate
      (for x from 0 below (width-of image))
      (format t "~X " (pixel-xy  image x y)))))

(defun pixval (r g b &optional (a 0))
  "Convert rgb values to a pixel uint32"
  (declare ((unsigned-byte 8) r g b a))
  (the (unsigned-byte 32)
    (logior
     (ash a 24)
     (ash b 16)
     (ash g 8)
     r)))

    


                              
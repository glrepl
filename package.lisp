
(defpackage :glrepl 
  (:use :cl :iterate :alexandria :trivial-gray-streams)
  (:export #:*bitmap-width* 
           #:*bitmap-height* 
           #:*font-pathname* 
           #:*font-data* 
           #:*font-images* 
           #:*glwindow*
           #:*console-render-debug-fn*
           #:*font-images* 
           #:*console*
           #:with-opengl
           #:name-of
           #:make-font
           #:destroy-font
           #:rgba-image
           #:glrepl-font
           #:bitmap-height-of
           #:bitmap-width-of
           #:pathname-of
           #:chain-of
           #:images-of
           #:glrepl-window
           #:glrepl-window-line
           #:current-line
           #:current-result-line
           #:current-line-as-string           
           #:current-cursor
           #:glrepl-window-mark
           #:add-line
           #:cursor-up
           #:cursor-down
           #:cursor-left
           #:cursor-right
           #:cursor-column
           #:add-char
           #:add-string
           #:del-char-left
           #:del-char-right
           #:set-mark
           #:texture-width-of
           #:texture-height-of
           #:text-height-of
           #:text-width-of
           #:win-height-of
           #:win-width-of
           #:font-of
           #:mark-of
           #:cursor-line-of
           #:top-line-of
           #:lines-of
           #:kills-of
           #:window-pixel-atxy
           #:viewport
           #:add-to-history
           #:previous-history
           #:next-history
           #:render-string
           #:render-console
           #:console-key-callback
           #:console-char-callback
           #:console-eval
           #:pixel
           #:update-image
           #:pixval))

(in-package :glrepl)
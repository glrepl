
(defpackage :glrepl-system
    (:use :common-lisp :asdf))

(in-package :glrepl-system)

(defsystem :glrepl
  :name "glrepl"
  :depends-on (:vecto :cffi :cl-glfw :cl-glfw-glu :cl-glfw-opengl :cl-glfw-opengl-version_1_1 :cl-glfw-opengl-version_1_2 :iterate :cffi :trivial-gray-streams :flexichain :alexandria)
  :serial t
  :components ((:file "package")               
               (:file "glrgba")
               (:file "glreader")
               (:file "glrepl")
               (:file "glrepl-window")
               (:file "glrepl-main")))

;; (defsystem :glrepl-tests
;;   :name "glrepl tests"
;;   :depends-on (:glrepl :flexichain :trivial-gray-streams)
;;   :serial t
;;   :components ((:file "glrepl-tests")))
